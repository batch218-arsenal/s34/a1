// This code will help us access contents of express module/package
	// A "module" is a software component or part of a program that contains one or more routines
// It also allows us to access methods and functions that we will use to easily create an app/server
// We store our express module to variable so we could easily access its keywords, functions, and methods.  

const express = require("express");

// This code creates an application using express / a.k.a. express aplication
	// app is our server
const app = express();

// For our application server to run, we need a port to listen to
const port = 3000;

// The purpose of this is to enable our application to read json data 
app.use(express.json());

// Allows your app to read data from forms
// By default, information received from the url can only be received as a string or an array
// By applying the option of "extended: true", this allows us to receive information in other data types, such as an object which we will use throughout our application
app.use(express.urlencoded({extended: true}));


// This route expects to receive a GET request at the URI/endpoint "/hello"
app.get("/hello", (request, response)=> {

	// This is the response that we will expect to receive if the GET method is successful with the right endpoint is successful
	response.send("GET method successful. \nHello from the /hello endpoint!")

});



// Tells our server/application to listen to the port
// If the port is accessed, we can run the server
// Returns a message to confirm that the server is running in the terminal
app.listen(port, () => console.log(`Server running at port ${port}`));

// POST
app.post("/hello", (request, response)=> {

	response.send(`Hello there ${request.body.firstName} ${request.body.lastName}!`)

});

let users = [];

app.post("/signup", (request, response)=>{
	if(request.body.username !== "" && request.body.password !== ""){
		users.push(request.body);
		console.log(users);
		response.send(`User ${request.body.username} successfully registered`);

		// Only the first response will be displayed
		response.send('Welcome again');
	} else{
		response.send("Please input BOTH username and password");
	}

});

// Syntax:

/*app.httpMethod("/signup", (request, response)=>{

	// code block

});

*/

// We use put method to update a document
app.put("/change-password", (request, response)=>{

	// we create a variable where we'll store our response
	let message;

	// Purpose of the loop is to check if the user is existing in users array, then update that user's password
	for(let i=0; i<users.length; i++){
		if(request.body.username == users[i].username){
			users[i].password = request.body.password;
			message = `User ${request.body.username}'s password has been updated`;
			break;
		} else {
			message = "User does not exist.";
		}
	}

	console.log(users);
	response.send(message);

});

// ACTIVITY SECTION

// 1.

app.get("/home", (request, response)=> {

	response.send("Welcome to the homepage")

});

// 2.

app.get("/users", (request, response)=> {

	response.send(users);

});

// 3.

app.delete("/delete-user", (request, response)=> {

	for(let i=0; i<users.length; i++){
		if(request.body.username == users[i].username){
			
			delete users[i];
			break;
		} 
	}

	response.send(users);

});